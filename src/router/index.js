import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/Home'
import PosterHome from '@/pages/PosterHome'
import Configurator from '@/pages/Configurator'
import Overview from '@/pages/Overview'
import Downloads from '@/pages/Downloads'
import MemeConfigurator from '@/pages/MemeConfigurator'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: '/',
  routes: [{
    name: 'home',
    path: '/',
    component: Home
  }, {
    name: 'poster-home',
    path: '/poster',
    component: PosterHome
  }, {
    name: 'open',
    path: '/poster/p/:code',
    redirect: to => `/poster/p/${to.params.code}/1`
  }, {
    name: 'step',
    path: '/poster/p/:code/:step',
    component: Configurator
  }, {
    name: 'overview',
    path: '/poster/overview',
    component: Overview,
    children: [{
      name: 'designer-details',
      path: '/poster/overview/:code'
    }]
  }, {
    name: 'downloads',
    path: '/downloads',
    component: Downloads
  }, {
    name: 'meme',
    path: '/meme',
    component: MemeConfigurator
  }]
})
