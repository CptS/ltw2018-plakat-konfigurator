// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import {
  Vuetify,
  VApp,
  VNavigationDrawer,
  VFooter,
  VList,
  VBtn,
  VCheckbox,
  VIcon,
  VGrid,
  VToolbar,
  VCard,
  VForm,
  VTextField,
  VTextarea,
  VSelect,
  VJumbotron,
  VDialog,
  VStepper,
  VProgressCircular,
  VProgressLinear,
  VMenu,
  VSwitch,
  VSnackbar,
  VSlider,
  VAlert,
  VTooltip,
  VChip,
  transitions
} from 'vuetify'
import '@/style/app.styl'

Vue.use(Vuetify, {
  theme: {
    primary: '#652480'
  },
  components: {
    VApp,
    VNavigationDrawer,
    VFooter,
    VList,
    VBtn,
    VCheckbox,
    VIcon,
    VGrid,
    VToolbar,
    VCard,
    VForm,
    VTextField,
    VTextarea,
    VSelect,
    VJumbotron,
    VDialog,
    VStepper,
    VProgressCircular,
    VProgressLinear,
    VMenu,
    VSwitch,
    VSnackbar,
    VSlider,
    VAlert,
    VTooltip,
    VChip,
    transitions
  }
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {App},
  template: '<App/>'
})
