# ltw2018-plakat-konfigurator

> Konfigurator für (Personen-)Plakate zur LTW/BzTW 2018 der Piratenpartei Bayern.

## Build
 
### Frontend

``` bash
# install dependencies
npm install

# build for production with minification
npm run build
```

### Backend

``` bash
# install dependencies
composer install -d public/api
```

## Run Dev Environment

### Frontend only

With hot deployment.

```bash
# serve with hot reload at localhost:8080
npm run dev
```

### Frontend and Backend

Requires built frontend.

```bash
php -S localhost:8000 -t public
```

## Deployment

Build the frontend and the backend and everything you need to deploy is placed in the directory `/public`.

### Requirements

1. URL rewriting:
    - `/api/**` → `/api/index.php`  
      (see https://www.slimframework.com/docs/v3/start/web-servers.html)
    - `/**` → `/index.html`  
      (see https://router.vuejs.org/guide/essentials/history-mode.html#example-server-configurations)
2. PHP:
    - Version: 7  
      (maybe older versions could also work but I only tested with this one)
    - Required modules: `php-mysql`, `php-gd`, `php-xml`
    - Required in directory `/api`
    - Large `upload_max_filesize` (`php.ini`)  
      (To allow upload of high resolution images it should be at least `20M`)
3. Database:
    - DBMS: MariaDB or MySQL
    - Database setup and scheme: `db_scheme.sql`
    - Database access configuration: `/api/_settings.php`
4. Filesystem:
    - Directory with read/write access
    - For security purpose is should be placed outside of the web root
    - Enough space for all uploaded images, preview images and final print files
      (and for sure a monitoring makes really sense here)
