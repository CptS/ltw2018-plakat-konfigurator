<?php
// Copy this file to ../../data/_settings.php
return array(
  'displayErrorDetails' => true,
  'db' => array(
    'host' => '127.0.0.1',
    'dbname' => 'perplakonf',
    'user' => 'root',
    'pass' => '123456'
  ),
  'filePath' => realpath(__DIR__ . '/files/'),
  'baseUrl' => 'http://localhost:8080',
  'senderEmail' => 'poster@localhost'
);
