<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/', function (Request $request, Response $response) {
  $response->getBody()->write("This is the API for the Per-Pla-Konf.");

  return $response->withJson(array('msg' => 'This is the API for the Per-Pla-Konf.', 'success' => true));
});

$app->post('/poster', function (Request $request, Response $response, array $args) {
  $poster = $request->getParsedBody();

  if ($poster && $poster['name'] && strlen($poster['name']) >= 5 && strlen($poster['name']) <= 42) {
    $repo = new PosterRepository($this->db, $this->settings);
    $poster = $repo->createPoster($poster['name']);

    return $response->withJson(array('poster' => $poster, 'success' => true));
  }
  return $response->withJson(array('msg' => 'Invalid name!', 'success' => false), 400);
});
$app->get('/poster/overview', function (Request $request, Response $response, array $args) {

  session_start();
  if ($_SESSION && $_SESSION['email']) {
    $repo = new PosterRepository($this->db, $this->settings);
    $poster = $repo->getPosterOverview();

    return $response->withJson(array('poster' => $poster, 'success' => true));
  }
  return $response->withJson(array('msg' => 'Login required', 'success' => false), 403);
});

$app->get('/poster/{code}', function (Request $request, Response $response, array $args) {

  $repo = new PosterRepository($this->db, $this->settings);
  $poster = $repo->getPoster($args['code']);

  if ($poster) {
    return $response->withJson(array('poster' => $poster, 'success' => true));
  }
  return $response->withJson(array('msg' => 'Poster not found!', 'success' => false), 404);
});

$app->put('/poster/{code}', function (Request $request, Response $response, array $args) {
  $posterData = $request->getParsedBody();

  if ($posterData && $args['code'] && strlen($args['code']) == 7) {
    $repo = new PosterRepository($this->db, $this->settings);
    $posterExists = $repo->getPoster($args['code']);

    if ($posterExists) {
      $repo->updatePoster($args['code'], $posterData);

      return $response->withJson(array('success' => true), 204);
    }
    return $response->withJson(array('msg' => 'Poster not found!', 'success' => false), 404);
  }
  return $response->withJson(array('msg' => 'Invalid poster id or request body!', 'success' => false), 400);
});

$app->put('/poster/{code}/download-info', function (Request $request, Response $response, array $args) {
  $downloadInfo = $request->getParsedBody();

  session_start();
  if ($_SESSION && $_SESSION['email']) {
    if ($downloadInfo && $args['code'] && strlen($args['code']) == 7) {
      $repo = new PosterRepository($this->db, $this->settings);
      $posterExists = $repo->getPoster($args['code']);

      if ($posterExists) {
        $repo->updatePosterDownloadInfo($args['code'], $downloadInfo);

        return $response->withJson(array('success' => true), 204);
      }
      return $response->withJson(array('msg' => 'Poster not found!', 'success' => false), 404);
    }
    return $response->withJson(array('msg' => 'Invalid poster id or request body!', 'success' => false), 400);
  }
  return $response->withJson(array('msg' => 'Login required', 'success' => false), 403);
});

$app->post('/poster/{code}/image', function (Request $request, Response $response, array $args) {
  if ($args['code'] && strlen($args['code']) == 7) {
    $files = $request->getUploadedFiles();
    $imageFile = $files['file'];

    $repo = new PosterRepository($this->db, $this->settings);
    $result = $repo->updatePosterImage($args['code'], $imageFile);
    return $response->withJson($result['response'], $result['status']);
  }
  return $response->withJson(array('msg' => 'Invalid poster id!', 'success' => false), 400);
});

$app->get('/poster/{code}/image/preview', function (Request $request, Response $response, array $args) {

  if ($args['code'] && strlen($args['code']) == 7) {
    $repo = new PosterRepository($this->db, $this->settings);
    $base64 = $repo->getPosterImagePreview($args['code']);

    return $response->withJson(array('preview' => $base64, 'success' => true), 200);
  }
  return $response->withJson(array('msg' => 'Invalid poster id!', 'success' => false), 400);
});

$app->get('/poster/{code}/messages', function (Request $request, Response $response, array $args) {

  if ($args['code'] && strlen($args['code']) == 7) {
    $repo = new PosterRepository($this->db, $this->settings);
    $messages = $repo->getPosterMessages($args['code']);

    $designer = false;
    session_start();
    if ($_SESSION && $_SESSION['email']) {
      $designer = true;
    }

    return $response->withJson(array('messages' => $messages, 'designer' => $designer, 'success' => true), 200);
  }
  return $response->withJson(array('msg' => 'Invalid poster id!', 'success' => false), 400);
});

$app->post('/poster/{code}/messages', function (Request $request, Response $response, array $args) {
  $messageData = $request->getParsedBody();

  if ($messageData && $messageData['message'] && strlen($messageData['message']) > 0
    && $args['code'] && strlen($args['code']) == 7) {
    $repo = new PosterRepository($this->db, $this->settings);

    $direction = 'out';
    session_start();
    if ($_SESSION && $_SESSION['email']) {
      $direction = 'in';
    }

    $repo->savePosterMessages($args['code'], $messageData['message'], $direction);

    return $response->withJson(array('success' => true), 204);
  }
  return $response->withJson(array('msg' => 'Invalid poster id or request body!', 'success' => false), 400);
});

$app->post('/poster/{code}/share', function (Request $request, Response $response, array $args) {
  $messageData = $request->getParsedBody();

  if ($messageData && $messageData['email'] && strlen($messageData['email']) > 0
    && $args['code'] && strlen($args['code']) == 7) {
    $repo = new PosterRepository($this->db, $this->settings);
    $repo->sharePoster($args['code'], $messageData['email']);

    return $response->withJson(array('success' => true), 204);
  }
  return $response->withJson(array('msg' => 'Invalid poster id or request body!', 'success' => false), 400);
});

$app->post('/poster/login', function (Request $request, Response $response, array $args) {
  $login = $request->getParsedBody();

  if ($login && $login['email'] && strlen($login['email']) > 0
    && $login['password'] && strlen($login['password']) > 0) {
    $repo = new PosterRepository($this->db, $this->settings);
    if ($repo->checkLogin(strtolower($login['email']), $login['password'])) {
      session_start();
      $_SESSION['email'] = strtolower($login['email']);
      return $response->withJson(array('success' => true, 'email' => $_SESSION['email']), 200);
    }
  }
  return $response->withJson(array('msg' => 'Invalid email or password!', 'success' => false), 400);
});

$app->get('/poster/login/info', function (Request $request, Response $response, array $args) {

  session_start();
  if ($_SESSION && $_SESSION['email']) {
    return $response->withJson(array('success' => true, 'email' => $_SESSION['email']), 200);
  }
  return $response->withJson(array('msg' => 'Invalid email or password!', 'success' => false), 400);
});

$app->post('/poster/logout', function (Request $request, Response $response, array $args) {

  session_start();
  if (session_destroy()) {
    return $response->withJson(array('success' => true), 200);
  }
  return $response->withJson(array('msg' => 'Logout failed!', 'success' => false), 400);
});

$app->put('/poster/login/password', function (Request $request, Response $response, array $args) {
  $passwords = $request->getParsedBody();

  session_start();
  if ($_SESSION && $_SESSION['email'] && $passwords
    && $passwords['currentPassword'] && strlen($passwords['currentPassword']) > 0
    && $passwords['newPassword'] && strlen($passwords['newPassword']) > 0
    && $passwords['newPasswordConfirm'] && strlen($passwords['newPasswordConfirm']) > 0
    && $passwords['newPassword'] == $passwords['newPasswordConfirm']) {
    $repo = new PosterRepository($this->db, $this->settings);
    if ($repo->checkLogin($_SESSION['email'], $passwords['currentPassword'])) {
      $repo->changePassword($_SESSION['email'], $passwords['newPassword']);
      return $response->withJson(array('success' => true), 200);
    }
  }
  return $response->withJson(array('msg' => 'One of the passed passwords is invalid!', 'success' => false), 400);
});

$app->post('/poster/login/create', function (Request $request, Response $response, array $args) {
  $body = $request->getParsedBody();

  session_start();
  if ($_SESSION && $_SESSION['email'] && $body && $body['email']) {
    $repo = new PosterRepository($this->db, $this->settings);
    $repo->createDesigner($body['email']);
    return $response->withJson(array('success' => true), 200);
  }

  return $response->withJson(array('msg' => 'E-Mail missing or invalid or not logged in!', 'success' => false), 400);
});
